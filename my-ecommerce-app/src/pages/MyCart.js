import { useState, useEffect, useContext } from 'react';
import { Link, useParams, useHistory } from 'react-router-dom';
import { Card, Button, Container, InputGroup, FormControl, Form, Table } from 'react-bootstrap';
import { add, total, list, get, exists, remove  } from 'cart-localstorage'
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function MyCart(){
	const [table, setTable] = useState([])

	let cartItems = list()

	useEffect(()=> {

		let list = cartItems.map((item, index) => {
			return (
		      <tr key={item.id}>
		          <td><Link to={`/products/${item.id}`}>{item.name}</Link></td>
		          <td>₱{item.price}</td>
		          <td>
		          {item.quantity}
		          </td>
		          <td>₱</td>
		          <td className="text-center">
		          	<Button variant="danger" onClick={remove(item.id)}>Remove</Button>
		          </td>
		      </tr>			
			)
		})
		setTable(list)

	}, [])






	return(
		<Container>
			<div className="my-4 text-center">
			<h2>Shopping Cart</h2>
			</div>
		<Table striped bordered hover responsive="lg">
			<thead className="bg-dark text-white">
				<tr>
					<th>Name</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Subtotal</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{table}
			</tbody>
		</Table>
		</Container>
		)
}