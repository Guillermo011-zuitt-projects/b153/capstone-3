import  Banner  from '../components/Banner'


export default function Home(){

	const data = {

		title: "Thrift En' Shop",
		content: "The best store for your Tech Needs!",
		destination: "/products",
		label: "Buy now!"
	}


	return(
		<>
			<Banner bannerProps={data}/>

		</>


		)
}