import { useState, useEffect, Component } from 'react'
import ProductCard from './ProductCard'
import { Container } from 'react-bootstrap'



export default function UserView({productsProp}){

	const [productsArr, setProductsArr] = useState([])

	useEffect(() => {
	const products = productsProp.map(product => {
		// console.log(courses)
		if(product.isActive){
		return <ProductCard key={product._id} productProp={product}/>
		}else{
		return null
		}
	})
	setProductsArr(products)
	}, [productsProp])



	return(
		<Container>
			<h1 className="text-center my-3">Products</h1>
			<div id="productsList">
			<div className="row">
			{productsArr}
			</div>
			</div>


		</Container>
		)
		
}