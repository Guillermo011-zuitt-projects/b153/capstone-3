import { useState, useEffect } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Container } from 'react-bootstrap'
import AppNavbar from './components/AppNavbar'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import Products from './pages/Products'
import MyCart from './pages/MyCart'
import SpecificProduct from './pages/SpecificProduct'
import { UserProvider } from "./UserContext"
import './App.css';

function App() {

 const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()

    setUser({
      id: null,
      isAdmin: null
    })
  }

  return(
  <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar/>
        <Route exact path="/" component={Home}/>
        <Route exact path="/login" component={Login}/>
        <Route exact path="/register" component={Register}/>
        <Route exact path="/products" component={Products}/>
        <Route exact path="/myCart" component={MyCart}/>
        <Route exact path="/products/:productId" component={SpecificProduct}/>
    </Router>
  </UserProvider>

      )
}

export default App;
